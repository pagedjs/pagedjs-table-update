// a valid html has only one thead and one tfoot per table
// https://html.spec.whatwg.org/multipage/tables.html#the-table-element

class tfoot extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    beforeParsed(content) {
        content.querySelectorAll('table').forEach(table => {
            let tableFooter = table.querySelector('tfoot');
            let tableHeader = table.querySelector('thead');

            if (tableFooter) {
                console.log(tableFooter);
                table.insertAdjacentElement('beforeend', tableFooter)
            }
            if (tableHeader) {
                console.log(tableHeader);
                table.insertAdjacentElement('afterbegin', tableHeader)

            }
        })
    }
}
Paged.registerHandlers(tfoot);